import numpy as np
import cv2
from PIL import Image
from pyzbar.pyzbar import decode
import serial
import time


port = serial.Serial("COM5", baudrate=9600, timeout=3.0)
port.flushInput()
port.flushOutput()

cap = cv2.VideoCapture(0)

while(True):

    # try:
    #     print(port.readline())
    # except port.SerialTimeoutException:
    #     print('Data could not be read')

    # Capture frame-by-frame
    ret, frame = cap.read()
    if not ret:
        continue

    # width = np.size(frame, 1)  # here is why you need numpy!  (remember to "import numpy as np")
    # height = np.size(frame, 0)

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) #COLOR_BGR2HSV

    # compute the Scharr gradient magnitude reprtesentation of the images
    # in both the x and y direction
    gradX = cv2.Sobel(gray, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
    gradY = cv2.Sobel(gray, ddepth=cv2.CV_32F, dx=0, dy=1, ksize=-1)

    # substract the y-gradient from the x-gradient
    gradient = cv2.subtract(gradX, gradY)
    gradient = cv2.convertScaleAbs(gradient)

    # blur and threshold the image
    blurred = cv2.blur(gradient, (9, 9))
    (_, thresh) = cv2.threshold(blurred, 225, 255, cv2.THRESH_BINARY)

    # construct a closing kernel and apply it to the thresholded image
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 7))
    closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

    # perform a series of erosions and dilations
    closed = cv2.erode(closed, None, iterations=4)
    closed = cv2.dilate(closed, None, iterations=4)

    # find the contours in the thresholded image, then sort the contours
    # by their area, keeping only the largest one
    (_, cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(cnts):
        c = sorted(cnts, key=cv2.contourArea, reverse=True)
        if len(c):
            c = c[0]

        # compute the rotated bounding box of the largest contour
        rect = cv2.minAreaRect(c)
        box = np.int0(cv2.boxPoints(rect))

        # draw a bounding box arounded the detected barcode and display the
        # image
        cv2.drawContours(frame, [box], -1, (0, 255, 0), 2)

    ############ PYZBAR DECODE ###############

    pil = Image.fromarray(gray)
    width, height = pil.size
    raw = pil.tobytes()

    data = decode(pil)
    if len(data) > 0:
        print(data[0].data)
        port.write(data[0].data)
        time.sleep(1)

    #############################

    # Display the resulting frame
    # cv2.imshow("Gradient", gradient)
    # cv2.imshow("Blurred", blurred)
    # cv2.imshow("Threshold", thresh)
    # cv2.imshow("Kernel", kernel)
    # cv2.imshow("Closing", closed)
    # cv2.imshow('Gray', gray)
    cv2.imshow('Original', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
