int incomingByte = 0;   // for incoming serial data
void setup() {
  pinMode(14, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // print the string when a newline arrives:
  if (Serial.available() > 0) {
    String str = Serial.readString();
    if (str.equals("https://twitter.com/slowsuperman")) {
      digitalWrite(14, HIGH);
    } else if (str.equals("vis-design")) {
      digitalWrite(14, LOW);
    }
   }
}
